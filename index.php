<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4 Activity</title>
</head>
<body>

	<h1>Building</h1>
	<p>
		The name of the building is <?php echo $bldg->getName(); ?>.
	</p>

	<p>
		The Caswynn Building has
		<?php echo $bldg->getFloors(); ?>
		floors.
	</p>

	<p>
		The Caswynn Building is located at 
		<?php echo $bldg->getAdd(); ?>.
	</p>

	<?php $bldg->setName('Caswynn Complex'); ?>
	<p>The name of the building has been changed to 
		<?php echo $bldg->getName();?>.
	</p>


	<h1>Condominium</h1>
	<p>
		The name of the condominium is <?php echo $condo->getCondoName(); ?>.
	</p>

	<p>
		The Enzo Condo has <?php echo $condo->getCondoFloors(); ?> floors.
	</p>

	<p>
		The Enzo Condo is located at 
		<?php echo $condo->getCondoAdd(); ?>.
	</p>

	<?php $condo->setCondoName('Enzo Tower'); ?>
	<p>
		The name of the condominium has been changed to <?php echo $condo->getCondoName();?>.
	</p>

</body>
</html>