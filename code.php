<?php
	class Building {

		protected $name;
		protected $floors;
		protected $address;


		public function __construct($name, $floors, $address){
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		}

		public function getName(){
			return $this->name;
		}

		public function setName($name){
			$this->name = $name;
		}

		public function getFloors(){
			return $this->floors;
		}

		public function getAdd(){
			return $this->address;
		}

	}


	class Condominium extends Building {

		public function getCondoName(){
			return $this->name;
		}

		public function setCondoName($name){
			$this->name = $name;
		}

		public function getCondoFloors(){
			return $this->floors;
		}

		public function getCondoAdd(){
			return $this->address;
		}
		
	}
		//Instantiation 
		$bldg = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
		
		//Instantiation 
		$condo = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
?>